import argparse
import numpy as np
import scipy.optimize as opt


kB = 0.001987 # Boltzmann's constant [kcal/molK]
area_per_lipid={'popc':68.3, 'dppc':63.0, 'popg':62.0, 'tlcl1':132.1, 'tlcl2':136.7}
def PARSER():
    parser = argparse.ArgumentParser(description='Reading two input files: one containing the paths to free energy profiles, number of monomers forming oligomer and the definition of a bound state; the second with the membrane composition. The free energy profiles should be expressed in kcal/mol and Angstrom, the unbound state should be at 0 kcal/mol')
    parser.add_argument('-f', '--inp',     metavar='N', type=str,   nargs='+', help = 'input file')
    parser.add_argument('-l', '--lip', metavar='N', type=str, nargs='+', help='file with lipid composition')
    parser.add_argument('-T', '--temp',    metavar='r', type=float, nargs=1,   help = 'temperature')
    parser.add_argument('-o', '--out', metavar='file.out', type=str, nargs=1, help='output name')
    parser.add_argument('-min', '--low', metavar='s', type=int, nargs=1, help='the lowest number of lipids per protein')
    parser.add_argument('-sp', '--spac', metavar='s', type=int, nargs=1, help='how the number of lipids is increased (e.g. 10 means compute for 10, 20, 30 etc)')
    parser.add_argument('-max', '--high', metavar='s', type=int, nargs=1, help='the highest number of lipids per protein')
    args = parser.parse_args()
    return args

def parse_input(input):
    return np.loadtxt(input, dtype={'names': ('filename', 'monomer_num', 'bound'), 'formats': ('S150','i2', 'f4')})

def lipid_input(lipid_comp):
    return np.loadtxt(lipid_comp, dtype={'names': ('lipid_name', 'content'), 'formats': ('S7', 'f4')})

def load_data(nazwa_pliku):
    dane = np.loadtxt(nazwa_pliku)
    bin_size = (dane[1,0]-dane[0,0])
    return [dane, bin_size]

def avg_area(lipid_composition):
    area = 0
    if np.size(lipid_composition) == 1:
        area=area_per_lipid[str(lipid_composition.astype(str))]
    else:
        for lip in lipid_composition:
            area += area_per_lipid[lip[0].astype(str)]*lip[1]
    return area

def fep_to_probabilites(prof):
    probs = np.exp(-1 * prof[:, 1] / kB / T)
    return np.stack((prof[:, 0], probs), axis=1)

def bound_unbound_states(profil, boundary, bin_s): # podac profil (data, two columns), boundary (user-defined bound state), bin_s (bin_size)
    end_prof = profil[-1, 0]
    iterator=0
    bound_state=0
    while profil[iterator, 0] < boundary:
        bound_state= bound_state + profil[iterator, 1]*bin_s
        iterator=iterator + 1
    iterator=0
    unbound_prof=0
    while profil[iterator, 0] < end_prof :
        if profil[iterator, 0] >= boundary:
            unbound_prof= unbound_prof + profil[iterator, 1]*bin_s
        iterator=iterator + 1
    return [bound_state, unbound_prof, end_prof]

def fractions(oligomer, lip_per_prot, profil, boundary, bound_state, unbound_prof,  end_prof):
    radius=np.sqrt(oligomer*lip_per_prot*area/2/np.pi) # 2* lip_per_prot - 10 lipids per dimer
    if radius > end_prof:
            return bound_state/(bound_state+unbound_prof+radius-end_prof)
    elif radius > boundary and radius <= end_prof:
            unb_prof=0
            iterator=0
            while dane[iterator, 0] < radius :
                    if dane[iterator, 0] >= boundary:
                            unb_prof= unb_prof + profil[iterator, 1]*bin_size
                    iterator=iterator + 1
            return bound_state/(bound_state+unb_prof)

def dimer_frac(vars):
    mono, dim = vars
    eq1 = k_2*mono**2 - dim
    eq2 = mono + 2*dim -1
    return [eq1, eq2]

def trimer_frac(vars):
    mono, dim, trim = vars
    eq1 = k_2 * mono**2 - dim
    eq2 = k_3 * mono*dim - trim
    eq3 = mono + 2*dim + 3*trim -1
    return [eq1, eq2, eq3]

def tetramer_frac(vars):
    mono, dim, trim, tet = vars
    eq1 = k_2 * mono**2 - dim
    eq2 = k_3 * mono*dim - trim
    eq3 = k_4 * mono * trim - tet
    eq4 = mono + 2*dim + 3*trim + 4*tet -1
    return [eq1, eq2, eq3, eq4]

# read input files
lipid_composition = lipid_input(PARSER().lip[0])
area = avg_area(lipid_composition)
input = parse_input(PARSER().inp[0])
T = PARSER().temp[0]
small_lip = PARSER().low[0]
lip_spac = PARSER().spac[0]
alot_of_lip = PARSER().high[0]

# prepare dictionaries with free energy profiles and with bin sizes
my_profiles = {}
my_bins = {}
bound_unbound = {}
for i in range(len(input['filename'])):
    prof_name = "profil%d" % input['monomer_num'][i]
    my_profiles[prof_name] = fep_to_probabilites(np.array(np.loadtxt(input[i]['filename'].astype(str))))
    bin_name = "bin%d" % input['monomer_num'][i]
    my_bins[bin_name] = (my_profiles[prof_name][1, 0] - my_profiles[prof_name][0, 0])
    # compute bound and unbound fractions for the FEP (only for the range of RC)
    b_unb_name = "b_unb_%d" % input['monomer_num'][i]
    bound_unbound[b_unb_name] = bound_unbound_states(my_profiles[prof_name], input[i]['bound'], my_bins[bin_name])

# generate array for outputs
results = np.zeros((len(range(small_lip, alot_of_lip+1, lip_spac)), len(input['filename'])+1))
# compute fraction of oligomers for each protein to lipid fraction
for n_lip in range(small_lip, alot_of_lip+1, lip_spac):
    frac = {}
    for i in range(len(input['filename'])):
        frac_name = "frac_%d" % input['monomer_num'][i]
        temp = input['monomer_num'][i]
        frac[frac_name] = fractions(temp, n_lip, my_profiles["profil%d" % temp], input['bound'][i], \
bound_unbound["b_unb_%d" % temp][0], bound_unbound["b_unb_%d" % temp][1], bound_unbound["b_unb_%d" % temp][2])
    # here I solved the equations from dimer up to tetramer; you can expand it if needed, the examples are given below :-)
    k_2 = frac['frac_2']/(1-frac['frac_2'])**2
    if len(input['filename'])+1 == 2:
        results[int((n_lip-small_lip)/lip_spac), :] = opt.fsolve(dimer_frac, (0.5, 0.25))
    elif len(input['filename'])+1 == 3:
        k_3 = frac['frac_3'] / (1 - frac['frac_3']) ** 2
        results[int((n_lip-small_lip)/lip_spac), :] = opt.fsolve(trimer_frac, (0.5, 0.19, 0.04))
    elif len(input['filename'])+1 == 4:
        k_3 = frac['frac_3'] / (1 - frac['frac_3']) ** 2
        k_4 = frac['frac_4'] / (1 - frac['frac_4']) ** 2
        results[int((n_lip-small_lip)/lip_spac), :] = opt.fsolve(tetramer_frac, (0.5, 0.15, 0.04, 0.01))
# save the results
lips = np.arange(small_lip, alot_of_lip+1, lip_spac).reshape(int(len(range(small_lip, alot_of_lip+1, lip_spac))), 1)
np.savetxt(PARSER().out[0], np.concatenate((lips,results), 1), fmt="%1.6f")
