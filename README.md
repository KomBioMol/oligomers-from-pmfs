# oligomers from pmfs
The script converts the free energy profiles of oligomerization (dimerization, trimerization, tetramerization (now monomer+trimer) etc) to fractions of each oligomer (monomer, dimer, trimer and so on).

# Inputs
For utilizing the script, one needs two inputs:
a) file with the following data in a row: name of the files containing free energy profiles, oligomerization state (e.g. 2 for dimer), a distance cutoff defining the bound state of the oligomer. 
Here, as an example you can check the file "profiles_testing.txt". The profiles should be provided in kcal/mol for energy and in Angstroms for distance.
IMPORTANT
The free energy profiles have to be shifted in such a way that their plateau region (for the unbound state) will be at ~0 kcal/mol. This is because the code extrapolates the value of dG to 0 kcal/mol
for the region not covered by the profile.
b) file with the lipid type and the membrane content of the selected lipid (from 0 to 1). Note - the dictionary for lipids is rather small for now, you can extend it by yourself by importing
the data from charmm-gui membrane builder. The dictionary is named "area_per_lipid" and is composed of a lipid name as a key and of the area per lipid as a value.

# how to use the script
To use the script, you can use the following command:
python oligomers_fraction.py -f "input a) - profiles" -l "input b) - membrane composition " -T "Temperature" -o "output name" -min "min" -max "max" -sp "lipid increase"
Where "min" and "max are the minimal and maximal number of lipids per protein, respectively and "lipid increase" defines how much lipids will be added to system between consecutive steps
(e.g. the number of membrane lipids will increase by 10 lipids per protein).
The example command is provided below:
python oligomers_fraction.py -f profiles_testing.txt -l lipid.txt -T 310 -o example_results.txt -min 50 -max 10000000000000 -sp 50000000000
Note that the provided profiles are far from being ideal. On the other hand, this is why they can be provided as an example even if they are unpublished.
